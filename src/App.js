import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import SearchPage from './components/SearchPage';
import ArtistPage from './components/ArtistPage';

function App() {
  const minutesToMS = (minutes) => {
    const m = Math.floor(minutes / 60);
    const s = minutes % 60;
    const formattedM = (m < 10 ? '0' : '') + m;
    const formattedS = (s < 10 ? '0' : '') + s;
    
    return formattedM + ':' + formattedS;
  }

  return (
    <>
      <nav className="text-muted border-bottom py-3 fs-3 mb-3 d-flex justify-content-between">
        <strong>RemoteMore Challenge</strong>
        <strong>Deezer</strong>
      </nav>
      <BrowserRouter>
        <div className="App">
          <Routes>
            <Route path="/" exact element={<SearchPage minutesToMS={minutesToMS}/>} />
            <Route path="/artist/:id" element={<ArtistPage minutesToMS={minutesToMS}/>} />
          </Routes>
        </div>
      </BrowserRouter>
    </>
  );
}

export default App;
