import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useParams, Link } from 'react-router-dom';

const ArtistPage = ({minutesToMS}) => {
  const { id } = useParams();
  const [artistInfo, setArtistInfo] = useState({});
  const [topTracks, setTopTracks] = useState([]);
  const [albums, setAlbums] = useState([]);
  const [loaders, setLoaders] = useState({
    tracks: false,
    albums: false,
  });
  const avatarDimensions = {
    width: "100%",
    height: "100%",
    position: "absolute",
    opacity: 0.4
  };

  useEffect(() => {
    const fetchArtistData = async () => {
      try {
        const response = await axios.get(
          `https://9mx33j53ua.execute-api.us-east-1.amazonaws.com/dev/artist/${id}`
        );
        setArtistInfo(response.data);

        setLoaders({...loaders, tracks: true});
        const topTracksResponse = await axios.get(
          `https://cors-anywhere.herokuapp.com/https://api.deezer.com/artist/${id}/top`
        );
        setTopTracks(topTracksResponse.data.data);
        setLoaders({...loaders, tracks: false});
        
        setLoaders({...loaders, albums: true});
        const albumsResponse = await axios.get(
          `https://cors-anywhere.herokuapp.com/https://api.deezer.com/artist/${id}/albums`
        );
        setAlbums(albumsResponse.data.data);
        setLoaders({...loaders, albums: false});
      } catch (error) {
        console.error('Error fetching artist data:', error);
      }
    };

    fetchArtistData();
  }, [id]);

  return (
    <div>
      <div className="card-group">
        <div className="card position-relative" style={{flexBasis: "25%"}}>
          <img src={artistInfo.picture_medium} alt="" style={avatarDimensions}/>
            <div class="card-body my-5 p-5">
              <h1 class="card-title">{artistInfo.name}</h1>
              <p class="card-text">{Number(artistInfo.nb_fan).toLocaleString()} Fans</p>
            </div>
        </div>
        <div className="card card-body">
          <h4>Top Tracks</h4>
          <ul className="list-group list-group-flush">
            {loaders.tracks
            ? (
              <li className="list-group-item px-0">
                <div class="spinner-border" role="status">
                  <span class="visually-hidden">Loading...</span>
                </div>
              </li>
            ) : topTracks.map((track, i) => (
              <li className="py-2 border-bottom px-0 d-flex justify-content-between" key={track.id}>
                <span>
                  <strong className="me-3">{i}</strong> {track.title}
                </span>
                <span className="text-muted">{minutesToMS(track.duration)}</span>
              </li>
            ))}
          </ul>
        </div>
      </div>
      
      <h2 className="my-4">Albums</h2>
      
      <div class="row">
        {loaders.albums
        ? (
          <div class="col">
            <div class="spinner-border" role="status">
              <span class="visually-hidden">Loading...</span>
            </div>
          </div>
        ) : albums.map((album) => (
          <div class="col-md-4">
            <div class="card shadow-sm mb-3">
              <img src="..." class="card-img-top bg-secondary" alt="..." style={{height: 200}}/>
              <div class="card-body">
                <h6 class="card-title">{album.title}</h6>
                <p class="card-text text-muted">{album.release_date.split('-')[0]}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ArtistPage;
