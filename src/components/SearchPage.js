import React, { useState } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

const SearchPage = ({minutesToMS}) => {
  const [searchResults, setSearchResults] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const handleSearch = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    setErrorMessage('');
    try {
      const response = await axios.get(
        `https://9mx33j53ua.execute-api.us-east-1.amazonaws.com/dev/search?q=${searchQuery}`
      );
      setSearchResults(response.data.data);
    } catch (error) {
      console.error('Error fetching search results:', error);
      setErrorMessage('We could not find anything at the moment');
    }
    setIsLoading(false);
  };

  return (
    <div>
      <form class="input-group mb-3" onSubmit={handleSearch}>
        <input
          type="text"
          value={searchQuery}
          placeholder="Search for tracks"
          className="form-control form-control-lg"
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <button type="submit" class="btn btn-outline-secondary">Search</button>
      </form>
      
      {!!errorMessage && <p className="text-danger">{errorMessage}</p>}

      {isLoading
        ? (
          <div className="card card-body shadow">
            <div class="spinner-border" role="status">
                <span class="visually-hidden">Loading...</span>
              </div>
          </div>
        ) : !!searchResults.length && (
          <div className="row">
            {searchResults.map((track) => (
              <div class="col-md-4">
                <div class="card shadow-sm mb-3">
                  <img src="..." class="card-img-top bg-secondary" alt="..." style={{height: 200}}/>
                  <div class="card-body">
                    <p class="card-text text-muted">{minutesToMS(track.duration)}</p>
                    <h5 class="card-title">{track.title}</h5>
                    <Link to={`/artist/${track.artist.id}`}>{track.artist.name}</Link>
                  </div>
                </div>
              </div>
            ))}
          </div>
        )
      }
    </div>
  );
};

export default SearchPage;
